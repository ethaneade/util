#include <util/kd_tree_hamming.hpp>
#include <vector>
#include <cstdlib>
#include <iostream>

template <int N>
struct Bits
{
    enum { NUM_WORDS = (N+31)/32 };
    uint32_t x[NUM_WORDS];
    size_t key;
    
    struct GetBits {

        void operator()(const Bits& b, uint32_t w[]) const {
            for (int i=0; i<NUM_WORDS; ++i)
                w[i] = b.x[i];
        }
    };

    struct GetKey {
        size_t operator()(const Bits& b) const {
            return b.key;
        }
    };
};

int main()
{
    const int N = 32;
    typedef Bits<N> Point;

    const int Z = 100000;
    const int TEST_Z = 1000;
    
    std::vector<Point> points(Z + TEST_Z);

    srand(47);
    for (size_t i=0; i<points.size(); ++i)
    {
        for (int j=0; j<Point::NUM_WORDS; ++j)
            points[i].x[j] = rand();
        points[i].key = i;
    }

    util::KDTreeHamming<N,size_t> tree;
    tree.build(&points[0], Z, Point::GetBits(), Point::GetKey());
    
    std::cerr << "height = " << tree.height() << std::endl;

    int hits = 0;
    int dist_sum = 0;
    int err_sum = 0;
    for (int i=0; i<TEST_Z; ++i) {
        util::KDTreeHamming<N,size_t>::Result result;
        if (!tree.find_nearest_bbf(points[Z+i], Point::GetBits(), N, 200, result)) {
            std::cerr << "find_nearest_bbf failed" << std::endl;
            continue;
        }
        //std::cerr << result.dist << std::endl;
        dist_sum += result.dist;
        unsigned int approx_dist = result.dist;

        if (!tree.find_nearest_bbf(points[Z+i], Point::GetBits(), N, Z, result)) {
            std::cerr << "find_nearest_bbf failed" << std::endl;
            continue;
        }

        if (result.dist == approx_dist)
            ++hits;

        err_sum += approx_dist - result.dist;
    }
    std::cerr << "avg = " << (double)dist_sum / TEST_Z << std::endl;
    std::cerr << "avg err to truth = " << (double)err_sum / TEST_Z << std::endl;
    std::cerr << "hits = " << hits << "/" << TEST_Z << std::endl;
    
    return 0;
}
