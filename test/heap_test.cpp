#include <util/heap.hpp>
#include <vector>
#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    util::Heap<int> h;
    h.init(1000);
    for (int i=0; i<1000; ++i) {
        h.push(rand());       
    }
    
    while (!h.empty()) {
        int x;
        h.pop(x);
        cout << x << endl;
    }
    
    return 0;
}
