#include <util/kd_tree.hpp>
#include <vector>
#include <cstdlib>
#include <iostream>

using std::cerr;
using std::endl;

template <class T, int N>
struct Point
{
    T x[N];

    struct GetValue {
        void operator()(const Point& p, T *y) const {
            for (int i=0; i<N; ++i)
                y[i] = p.x[i];
        }
    };
};

template <class X>
struct GetKey {
    const X* const base;
    GetKey(const X* base_) : base(base_) {}

    size_t operator()(const X& x) const { return &x - base; }
};

int main()
{
    typedef float T;
    const int N = 4;
    
    std::vector<Point<T,N> > points(100000);

    for (size_t i=0; i<points.size(); ++i)
    {
        for (int j=0; j<N; ++j)
            points[i].x[j] = (T)(drand48() * 256);
    }

    const size_t num_in_tree = points.size() * 9 / 10;
    
    typedef util::KDTree<T,N,size_t> Tree;
    Tree tree;
    tree.build(&points[0], num_in_tree, Point<T,N>::GetValue(), GetKey<Point<T,N> >(&points[0]));
    cerr << "height after build = " << tree.height() << endl;

    tree.clear();
    for (size_t i=0; i<num_in_tree; ++i) {
        tree.add(i, points[i].x);
    }
    cerr << "height after adds = " << tree.height() << endl;
    tree.rebuild();
    cerr << "height after rebuild = " << tree.height() << endl;

    int num_hits = 0, num_queries = 0;
    double sum_approx_ssd = 0, sum_true_ssd = 0;
    for (size_t i=num_in_tree; i<points.size(); ++i) {
        const int K = 2;
        Tree::Result r[K], true_r[K];
        int nr = tree.find_nearest_bbf(points[i].x, 1e100, 100, r, K);
        int true_nr = tree.find_nearest_bbf(points[i].x, 1e100, tree.size(), true_r, K);

        if (nr > 0 && true_nr > 0) {
            if (r[0].key == true_r[0].key)
                ++num_hits;

            sum_approx_ssd += r[0].ssd;
            sum_true_ssd += true_r[0].ssd;
        }
        ++num_queries;
        if (num_queries % 1000 == 0)
            cerr << num_queries << endl;
    }
    cerr << num_hits << " hits of " << num_queries << " queries" << endl;
    cerr << sum_approx_ssd/num_queries << " avg approx ssd" << endl;
    cerr << sum_true_ssd/num_queries << " avg true ssd" << endl;
    
    return 0;
}
