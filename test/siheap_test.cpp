#include <util/small_integer_heap.hpp>
#include <cstdlib>
#include <iostream>

using namespace util;
using namespace std;

int main()
{
    const int MaxKey = 128;
    SmallIntegerHeap<float,MaxKey> heap;


    for (int i=0; i<1000000; ++i) {
        float val = drand48();
        int key = (int)(val * (MaxKey+0.5f));
        heap.push(val, key);        
    }
    
    cout << heap.size() << endl;

    int last = 0;
    while (!heap.empty()) {
        float val;
        int key = heap.pop(val);
        assert(key >= last);
        last = key;
        int comp_key = (int)(val * (MaxKey+0.5f));
        assert(key == comp_key);
    }
    cout << heap.size() << endl;
    return 0;
}
