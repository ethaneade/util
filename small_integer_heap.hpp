#pragma once

#include <util/pool.hpp>
#include <stdint.h>
#include <cassert>

namespace util {

    template <class Value, unsigned int MaxKey=255>
    class SmallIntegerHeap
    {
    private:
        struct Entry {
            Value value;
            Entry *next;
        };
        
        Pool<Entry> entry_pool;

        typedef uint32_t Word;
        enum {
            BITS_PER_WORD = 32,
            NUM_WORDS = (MaxKey + 1 + (BITS_PER_WORD-1)) / BITS_PER_WORD
        };

        Word not_empty[NUM_WORDS];
        Entry *slot[MaxKey+1];

        size_t count;

        unsigned int top_slot() const
        {
            for (int i=0; i<NUM_WORDS; ++i) {
                if (!not_empty[i])
                    continue;
                return i*BITS_PER_WORD + __builtin_clz(not_empty[i]);
            }
            assert(!empty());
            return MaxKey+1;
        }
        
    public:

        SmallIntegerHeap()
        {
            clear();
        }

        void clear()
        {
            entry_pool.clear();
            count = 0;
            for (int i=0; i<NUM_WORDS; ++i)
                not_empty[i] = 0;            
            for (unsigned int i=0; i<=MaxKey; ++i)
                slot[i] = 0;
        }

        bool empty() const { return count == 0; }
        size_t size() const { return count; }

        void push(const Value& v, unsigned int key)
        {
            assert(key <= MaxKey);

            Entry *e = entry_pool.alloc();
            e->value = v;
            e->next = slot[key];
            slot[key] = e;
            not_empty[key/BITS_PER_WORD] |= 1u << (BITS_PER_WORD-1 - (key % BITS_PER_WORD));
            ++count;
        }
        
        const Value& top() const
        {
            assert(!empty());

            unsigned int ts = top_slot();
            return slot[ts]->value;
        }

        unsigned int pop(Value &v)
        {
            assert(!empty());

            unsigned int ts = top_slot();
            Entry *e = slot[ts];
            v = e->value;
            slot[ts] = e->next;
            if (!slot[ts]) {
                not_empty[ts/BITS_PER_WORD] &= ~(1u << (BITS_PER_WORD-1 - (ts % BITS_PER_WORD)));
            }
            entry_pool.release(e);
            --count;
            return ts;
        }
        
        unsigned int pop()
        {
            Value v;
            return pop(v);
        }
    };

}
