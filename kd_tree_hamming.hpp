#pragma once

#include <stdint.h>
#include <util/pool.hpp>
#include <util/small_integer_heap.hpp>
#include <util/hamming.hpp>

namespace util {

    template <int N, class Key>
    class KDTreeHamming
    {
    private:
        typedef uint32_t Word;
        enum {
            BITS_PER_WORD = 32,
            NUM_WORDS = (N+BITS_PER_WORD-1)/BITS_PER_WORD
        };
        struct Value {
            Word word[NUM_WORDS];
            Key key;
            bool bit(unsigned int b) const { return word[b/BITS_PER_WORD] & (1u<<(b % BITS_PER_WORD)); }
        };

        struct Node {
            int bit;
            bool is_leaf() const { return bit < 0; }            
            union {
                struct {
                    Node *left, *right;
                } internal;
                struct {
                    unsigned int count;
                    unsigned int capacity;
                    Value *values;
                } leaf;
            };

            Node() : bit(-1) { leaf.values = 0; leaf.count = leaf.capacity = 0; }

            void reserve_leaf(unsigned int size) {
                if (size <= leaf.capacity)
                    return;
                
                unsigned int n = leaf.capacity*3/2;
                if (n < size)
                    n = size;
                Value *v = new Value[n];
                if (leaf.values) {
                    for (unsigned int i=0; i<leaf.count; ++i)
                        v[i] = leaf.values[i];
                    delete[] leaf.values;
                }
                leaf.values = v;
                leaf.capacity = n;
            }
        };

        
        Pool<Node> node_pool;
        size_t max_per_leaf;

#if PROFILE_KDTREEHAMMING
        unsigned long t_leaf;
        unsigned long t_dimvar;
        unsigned long t_findsplit;
        unsigned long t_partition;
        void t_clear() {
            t_leaf = t_dimvar = t_findsplit = t_partition = 0;
        }

        Timer timer;
#define PROFILE_TIMING(x) x
#else
#define PROFILE_TIMING(x)        
#endif
        struct GetStridedWord
        {
            const Value *values;
            const int word, step;
            GetStridedWord(const Value *v, int w, int s)
                : values(v), word(w), step(s) {}

            uint32_t operator[](int i) const { return values[i*step].word[word]; }
        };
        
        void build_node(Node *node, Value *v, size_t n)
        {
            PROFILE_TIMING(timer.restart());
            if (n <= max_per_leaf) {
                node->bit = -1;

                size_t count = n < 8 ? 8 : n;
                node->leaf.values = new Value[count];
                node->leaf.count = n;
                node->leaf.capacity = count;
                for (size_t i=0; i<n; ++i)
                    node->leaf.values[i] = v[i];
                PROFILE_TIMING(t_leaf += timer.elapsed());
                return;
            }

            int num_set[NUM_WORDS * BITS_PER_WORD];
            int num_samples;
            {
                int step = 1;
                num_samples = n;
                if (n > 1024) {
                    step = n / 1024;
                    num_samples = 1024;
                }

                for (int w=0; w<NUM_WORDS; ++w) {
                    count_set_bits(GetStridedWord(v, w, step), num_samples, &num_set[w*32]);
                }
            }
            PROFILE_TIMING(t_dimvar += timer.restart());
            unsigned int argbest = 0;
            int bestval = num_samples/2;
            for (int i=0; i<N; ++i) {
                int val = num_set[i] - (num_samples/2);
                val = val < 0 ? -val : val;
                if (val < bestval) {
                    bestval = val;
                    argbest = i;
                }
            }
            PROFILE_TIMING(t_findsplit += timer.restart());
            
            if (bestval == num_samples/2) {
                node->bit = -1;

                size_t count = n < 8 ? 8 : n;
                node->leaf.values = new Value[count];
                node->leaf.count = n;
                node->leaf.capacity = count;
                for (size_t i=0; i<n; ++i)
                    node->leaf.values[i] = v[i];
                PROFILE_TIMING(t_leaf += timer.elapsed());
                return;
            }

            const int wi = argbest / BITS_PER_WORD;
            const int bi = 1u << (argbest % BITS_PER_WORD); 
            Value *it = v, *end = v+n;
            while (it < end) {
                if (it->word[wi] & bi) {
                    Value tmp = *--end;
                    *end = *it;
                    *it = tmp;
                } else {
                    ++it;
                }        
            }
            PROFILE_TIMING(t_partition += timer.restart());
            
            node->bit = argbest;
            
            node->internal.left = node_pool.alloc();
            node->internal.right = node_pool.alloc();

            size_t num_left = it-v;
            build_node(node->internal.left, v, num_left);
            build_node(node->internal.right, it, n - num_left);
        }

        void add(Node *node, const Value& v)
        {
            if (!node->is_leaf()) {
                if (v.bit(node->bit))
                    add(node->internal.right, v);
                else
                    add(node->internal.left, v);
                return;
            }

            node->reserve_leaf(node->leaf.count + 1);
            node->leaf.values[node->leaf.count++] = v;
                
            if (node->leaf.count <= max_per_leaf)
                return;

            Value *vs = node->leaf.values;
            build_node(node, vs, node->leaf.count);
            delete[] vs;
        }

        bool remove(Node *node, const Value &v)
        {
            if (!node->is_leaf()) {
                if (v.bit(node->bit))
                    return remove(node->internal.right, v);
                else
                    return remove(node->internal.left, v);
            }
            
            for (unsigned int i=0; i<node->leaf.count; ++i) {
                if (node->leaf.values[i].key == v.key) {
                    node->leaf.values[i] = node->leaf.values[--node->leaf.count];
                    return true;
                }
            }
            return false;
        }

        static Value* get_all(const Node *node, Value *vs)
        {
            if (node->is_leaf()) {
                for (unsigned int i=0; i<node->leaf.count; ++i)
                    *vs++ = node->leaf.values[i];
                return vs;
            }
            vs = get_all(node->internal.left, vs);
            vs = get_all(node->internal.right, vs);
            return vs;
        }

        void destroy(Node *node)
        {
            if (node->is_leaf()) {
                delete[] node->leaf.values;
                return;
            }
            destroy(node->internal.left);
            node_pool.release(node->internal.left);
            
            destroy(node->internal.right);
            node_pool.release(node->internal.right);            
        }
                
        Node *root;
        size_t count;

        size_t height(const Node *node) const
        {
            if (node->is_leaf())
                return 1;
            size_t hl = height(node->internal.left);
            size_t hr = height(node->internal.right);
            return 1 + (hl > hr ? hl : hr);
        }
        
    public:
        KDTreeHamming()
        {
            max_per_leaf = 8;
            count = 0;
            root = 0;
        }
        
        ~KDTreeHamming()
        {
            clear();
        }

        void clear()
        {
            if (root) {
                destroy(root);
                root = 0;
            }
            count = 0;
        }
        
        size_t size() const { return count; }
        bool empty() const { return count == 0; }
        size_t height() const
        {
            return empty() ? 0 : height(root);
        }
        
        template <class It, class GetBits, class GetKey>
        void build(It it, size_t n, const GetBits& get_bits, const GetKey& get_key, size_t max_per_leaf_=8)
        {
            clear();

            max_per_leaf = max_per_leaf_;

            Value *values = new Value[n];
            for (size_t i=0; i<n; ++i, ++it) {
                get_bits(*it, values[i].word);
                values[i].key = get_key(*it);
            }

#if PROFILE_KDTREEHAMMING
            t_clear();
#endif
            root = node_pool.alloc();
            build_node(root, values, n);

#if PROFILE_KDTREEHAMMING
            std::cerr << "for n = " << n << ": "
                      << "leaf = " << t_leaf*1e-3
                      << ", var = " << t_dimvar*1e-3
                      << ", split = " << t_findsplit*1e-3
                      << ", part = " << t_partition*1e-3
                      << std::endl;
#endif
            
            delete[] values;
            
            count = n;
        }

        template <class T, class GetBits>
        void add(const T& t, Key key, const GetBits& get_bits)
        {
            Value v;
            get_bits(t, v.word);
            v.key = key;

            if (empty()) {
                root = node_pool.alloc();
                root->bit = -1;
                root->leaf.count = root->leaf.capacity = 0;
                root->leaf.values = 0;
            }
            add(root, v);
            ++count;
        }

        template <class T, class GetBits>
        bool remove(const T& t, Key key, const GetBits& get_bits)
        {
            if (empty())
                return false;
            
            Value v;
            get_bits(t, v.word);
            v.key = key;

            if (remove(root, v)) {
                --count;
                return true;
            } else                
                return false;
        }
        
        void rebuild(size_t max_per_leaf_=8)
        {
            if (empty())
                return;

            const size_t n = count;
            Value *values = new Value[n];
            get_all(root, values);
            clear();
            max_per_leaf = max_per_leaf_;
            root = node_pool.alloc();
            build_node(root, values, n);
            delete[] values;            
            count = n;
        }

        struct Result
        {
            Key key;
            unsigned int dist;
        };

        static void update_results(Result rs[], int maxn, int &n,
                                   const Result &r)
        {
            int k;
            if (n == maxn) {
                if (rs[maxn-1].dist <= r.dist)
                    return;
                k = maxn-1;
            } else {
                k = n++;
            }
            
            while (k > 0 && r.dist < rs[k-1].dist) {
                rs[k] = rs[k-1];
                --k;
            }
            rs[k] = r;
        }
        
        template <class T, class GetBits>
        bool find_nearest_bbf(const T& x, const GetBits& get_bits,
                              unsigned int max_dist,
                              unsigned int max_tries,
                              Result& result) const
        {
            return find_nearest_bbf(x, get_bits, max_dist, max_tries,
                                    &result, 1) != 0;
        }

        template <class T, class GetBits>
        int find_nearest_bbf(const T& x, const GetBits& get_bits,
                             unsigned int max_dist,
                             unsigned int max_tries,
                             Result results[], int max_results) const
        {
            const KDTreeHamming *me = this;
            return find_nearest_bbf(&me, 1, x, get_bits,
                                    max_dist, max_tries,
                                    results, max_results) != 0;
        }
        
        
        template <class T, class GetBits, class Tree>
        static int find_nearest_bbf(const Tree *trees[], int num_trees,
                                    const T& x, const GetBits& get_bits,
                                    unsigned int max_dist,
                                    unsigned int max_tries,
                                    Result results[], int max_results)
        {
            SmallIntegerHeap<const Node*,N> heap;
            for (int i=0; i<num_trees; ++i) {
                if (!trees[i]->empty())
                    heap.push(trees[i]->root,0);
            }
            if (heap.empty())
                return 0;

            Value v;
            get_bits(x, v.word);

            int nr = 0;
            unsigned int best_dist = max_dist + 1;
            
            unsigned int tries = 0;
            while (!heap.empty())
            {
                const Node *node;
                unsigned int dist_lb = heap.pop(node);
                if (dist_lb >= best_dist)
                    break;

                while (!node->is_leaf()) {
                    const Node *worse;
                    if (v.bit(node->bit)) {
                        worse = node->internal.left;
                        node = node->internal.right;
                    } else {
                        worse = node->internal.right;
                        node = node->internal.left;
                    }
                    if (dist_lb + 1 < best_dist)
                        heap.push(worse, dist_lb + 1);
                }

                for (unsigned int i=0; i<node->leaf.count; ++i) {
                    unsigned int dist = 0;
                    for (int j=0; j<NUM_WORDS; ++j)
                        dist += hamming_distance(node->leaf.values[i].word[j], v.word[j]);
                    
                    if (dist >= best_dist)
                        continue;
                    
                    Result r = {node->leaf.values[i].key, dist};
                    update_results(results, max_results, nr, r);
                }
                if (nr == max_results)
                    best_dist = results[nr-1].dist;
                
                tries += node->leaf.count;
                if (tries >= max_tries)
                    break;
            }
            return nr;
        }
            
    };

}
