#pragma once

#include <util/pool.hpp>
#include <util/heap.hpp>
#include <cassert>

namespace util {

    template <typename T, int N, class Key>
    class KDTree
    {
    private:

        struct Value {
            T x[N];
            Key key;
        };
        
        struct Node {
            int split_dim;

            Node() {
                split_dim = -1;
                leaf.count = leaf.capacity = 0;
                leaf.values = 0;
            }
            
            union {
                struct {
                    T split;                   
                    Node *left, *right;
                } internal;
                
                struct {
                    unsigned int count;
                    unsigned int capacity;
                    Value *values;
                } leaf;
            };

            bool is_leaf() const { return split_dim == -1; }

            void make_leaf(const Value vals[], unsigned int n) {
                split_dim = -1;
                leaf.count = n;
                leaf.capacity = n < 8 ? 8 : n;
                leaf.values = new Value[leaf.capacity];
                for (unsigned int i=0; i<n; ++i)
                    leaf.values[i] = vals[i];
                return;
            }

            void destroy() {
                if (is_leaf()) {
                    delete[] leaf.values;
                    leaf.values = 0;
                } else {
                    internal.left->destroy();
                    internal.right->destroy();
                }
            }
        };

        Pool<Node> node_pool;
        unsigned int max_per_leaf;
        
        void build(Node *node, Value vals[], unsigned int n)
        {
            if (n <= max_per_leaf)
            {
                node->make_leaf(vals, n);
                return;
            }

            double sum[N];
            double sum_sq[N];
            for (int i=0; i<N; ++i)
                sum[i] = sum_sq[i] = 0;

            unsigned int step = n < 256 ? 1 : n / 256;
            int count = 0;
            for (unsigned int i=0; i<n; i += step) {
                const T *x = vals[i].x;
                for (int j=0; j<N; ++j) {
                    double xj = (double)x[j];
                    sum[j] += xj;
                    sum_sq[j] += xj*xj;
                }
                ++count;
            }

            double inv_n = 1.0/count;
            double max_var = 0;
            int argmax = -1;
            double argmax_mu = 0;
            
            for (int i=0; i<N; ++i) {
                double mu = inv_n * sum[i];
                double var = sum_sq[i] * inv_n - mu*mu;
                if (var > max_var) {
                    max_var = var;
                    argmax = i;
                    argmax_mu = mu;
                }
            }
            
            assert(argmax >= 0);
            
            unsigned int end = n;
            unsigned int begin = 0;

            while (begin < end) {
                if (vals[begin].x[argmax] < argmax_mu)
                    ++begin;
                else {
                    Value tmp = vals[--end];
                    vals[end] = vals[begin];
                    vals[begin] = tmp;                    
                }
            }

            if (begin == 0 || begin == n) {
                node->make_leaf(vals, n);
                return;
            }

            node->split_dim = argmax;
            node->internal.split = (T)argmax_mu;
            node->internal.left = node_pool.alloc();
            node->internal.right = node_pool.alloc();            
            build(node->internal.left, vals, begin);
            build(node->internal.right, vals + begin, n - begin);
        }

        void add(Node *node, const Value& val)
        {
            if (node->split_dim >= 0) {
                if (val.x[node->split_dim] < node->internal.split)
                    add(node->internal.left, val);
                else
                    add(node->internal.right, val);
                return;
            }

            unsigned int count = node->leaf.count;
            if (count + 1 <= max_per_leaf) {
                if (count < node->leaf.capacity) {
                    node->leaf.values[count] = val;
                    ++node->leaf.count;
                    return;
                }
            }

            Value *vs = new Value[count + 1];
            for (unsigned int i=0; i<count; ++i)
                vs[i] = node->leaf.values[i];
            vs[count++] = val;

            if (count <= max_per_leaf) {
                node->leaf.count = count;
                node->leaf.capacity = count;
                delete[] node->leaf.values;
                node->leaf.values = vs;
                return;
            }

            delete[] node->leaf.values;
            node->leaf.values = 0;
            
            build(node, vs, count);
            delete[] vs;
        }

        Node *root;
        unsigned int count;

        unsigned int height(const Node *n) const {
            if (n->split_dim == -1)
                return 1;
            unsigned int hl = height(n->internal.left);
            unsigned int rl = height(n->internal.right);
            return 1 + (hl > rl ? hl : rl);
        }

        unsigned int get_all(const Node* node, Value *v) const {
            if (node->split_dim == -1) {
                for (unsigned int i=0; i<node->leaf.count; ++i)
                    v[i] = node->leaf.values[i];
                return node->leaf.count;
            }
            unsigned int num_left = get_all(node->internal.left, v);
            v += num_left;
            unsigned int num_right = get_all(node->internal.right, v);
            return num_left + num_right;
        }

    public:
        KDTree() : max_per_leaf(8), root(0), count(0) {}
        ~KDTree() {
            if (root)
                root->destroy();
        }

        unsigned int height() const {
            return root ? height(root) : 0;
        }

        unsigned int size() const { return count; }
        bool empty() const { return count == 0; }

        void clear() {
            if (empty())
                return;
            root->destroy();
            root = 0;            
            node_pool.clear();
            count = 0;
        }

        template <class It, class GetValue, class GetKey>
        void build(It it, unsigned int n, const GetValue& get_val, const GetKey& get_key, unsigned int max_per_leaf_=8)
        {
            clear();
            
            root = node_pool.alloc();
            
            max_per_leaf = max_per_leaf_;
            Value *vals = new Value[n];
            for (size_t i=0; i<n; ++i, ++it) {
                get_val(*it, vals[i].x);
                vals[i].key = get_key(*it);
            }

            build(root, vals, n);
            count = n;
            
            delete[] vals;
        }

        void rebuild(unsigned int max_per_leaf_=8)
        {
            max_per_leaf = max_per_leaf_;
            if (empty())
                return;

            unsigned int n = count;
            Value *vals = new Value[n];
            get_all(root, vals);

            clear();

            root = node_pool.alloc();
            build(root, vals, n);
            count = n;
            delete[] vals;
        }

        void add(Key key, const T x[N])
        {
            if (empty()) {
                root = node_pool.alloc();
            }

            Value v;
            v.key = key;
            for (int i=0; i<N; ++i)
                v.x[i] = x[i];
            add(root, v);
            ++count;
        }

        struct Result {
            int key;
            double ssd;
        };

        static void update_results(Result *rs, int &nr, int maxnr, const Result &r)
        {
            int i = nr;
            while (i > 0 && r.ssd < rs[i-1].ssd)
                --i;
            if (i == maxnr)
                return;

            if (nr < maxnr)
                ++nr;

            for (int j=i+1; j<nr; ++j)
                rs[j] = rs[j-1];
            rs[i] = r;
        }

        struct Path
        {
            double ssd_lower_bound;
            double ssd_for_dim[N];
            Node *node;

            struct LessByLowerBound {
                bool operator()(const Path *a, const Path *b) const {
                    return a->ssd_lower_bound < b->ssd_lower_bound;
                }
            };
        };
        
        static int find_nearest_bbf(const KDTree<T,N,Key> * const trees[], int num_trees,
                                    const T x[N],
                                    double max_ssd, unsigned int max_tries,
                                    Result results[], int max_results)
        {
            Pool<Path> path_pool;
            Heap<Path*,typename Path::LessByLowerBound> path_heap;
            int total_nodes = 0;
            for (int i=0; i<num_trees; ++i) {
                total_nodes += trees[i]->node_pool.num_alive();
            }
            
            path_heap.init(total_nodes);
            
            for (int i=0; i<num_trees; ++i) {
                if (trees[i]->empty())
                    continue;
                Path *p = path_pool.alloc();
                p->ssd_lower_bound = 0;
                for (int j=0; j<N; ++j)
                    p->ssd_for_dim[j] = 0;
                p->node = trees[i]->root;
                path_heap.push(p);
            }

            double upper_bound = max_ssd;

            int nr = 0;
            unsigned int tries = 0;
            
            while (true) {
                Path *p;
                if (!path_heap.pop(p))
                    break;

                if (p->ssd_lower_bound > upper_bound)
                    continue;
                
                Node *node = p->node;
                while (!node->is_leaf()) {
                    const int dim = node->split_dim;
                    const double d = x[dim] - node->internal.split;
                    Node *worse;
                    if (d < 0) {
                        worse = node->internal.right;
                        node = node->internal.left;
                    } else {
                        worse = node->internal.left;
                        node = node->internal.right;                        
                    }

                    const double dd = d*d;
                    const double new_lb = p->ssd_lower_bound + dd - p->ssd_for_dim[dim];
                    if (new_lb < upper_bound) {
                        Path *wp = path_pool.alloc();
                        *wp = *p;
                        wp->node = worse;
                        wp->ssd_for_dim[dim] = dd;
                        wp->ssd_lower_bound = new_lb;
                        path_heap.push(wp);
                    }
                }
                path_pool.release(p);

                for (unsigned int i=0; i<node->leaf.count; ++i) {
                    double ssd = 0;
                    const Value &v = node->leaf.values[i];
                    for (int j=0; j<N; ++j) {
                        double d = x[j] - v.x[j];
                        ssd += d*d;
                    }
                    if (ssd >= upper_bound)
                        continue;
                    
                    Result r;
                    r.key = v.key;
                    r.ssd = ssd;
                    update_results(results, nr, max_results, r);
                }

                tries += node->leaf.count;
                if (tries >= max_tries)
                    break;
                
                if (nr == max_results)
                    upper_bound = results[max_results-1].ssd;
            }
            return nr;
        }

        int find_nearest_bbf(const T x[N],
                             double max_ssd, unsigned int max_tries,
                             Result results[], int max_results) const
        {
            KDTree<T,N,Key> const * const me[1] = {this};
            return find_nearest_bbf(me, 1, x, max_ssd, max_tries, results, max_results);
        }        
    };

}
