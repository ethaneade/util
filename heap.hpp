#pragma once

#include "comparator.hpp"

namespace util {
    
    template <typename T, class Comp=LessThan>
    class Heap
    {
    public:
        Heap(const Comp &c = Comp())
            : cmp(c), x(0), cap(0), count(0) {}
        
        Heap(int n, const Comp &c = Comp())
            : cmp(c), count(0)
        {
            init(n);
        }        
        
        ~Heap()
        {
            delete[] x;
        }

        bool init(int max_size)
        {            
            delete[] x;
            cap = max_size;                        
            if (cap > 0) {
                x = new T[cap];
            } else {
                x = 0;
            }
            count = 0;
            return true;
        }

        int size() const { return count; }
        int capacity() const { return cap; }
        bool empty() const { return count == 0; }

        void clear()
        {
            count = 0;
        }
        
        bool pop(T &v)
        {
            if (count == 0)
                return false;
            v = x[0];
            x[0] = x[--count];
            heapify_down();
            return true;
        }
        
        bool push(const T& v)
        {
            if (count == cap)
                return false;
            x[count++] = v;
            heapify_up();
            return true;
        }

        void swap(Heap<T,Comp> &h)
        {
            Heap<T,Comp>::swap(cmp, h.cmp);
            Heap<T,Comp>::swap(x, h.x);
            Heap<T,Comp>::swap(cap, h.cap);
            Heap<T,Comp>::swap(count, h.count);
        }
        
    private:

        void heapify_down()
        {
            int i=0;            
            while (2*i+1<count) {
                int c0 = 2*i+1;
                int c1 = c0 + 1;
                if (c1 == count) {
                    if (cmp(x[i], x[c0]))
                        return;
                    swap_entries(i, c0);
                    i = c0;
                    continue;
                }                              
                int next = c1;
                if (cmp(x[i], x[c0])) {
                    if (cmp(x[i], x[c1]))
                        return;
                } else if (cmp(x[i], x[c1])){
                    next = c0;
                } else if (cmp(x[c0], x[c1])) {
                    next = c0;
                }
                
                swap_entries(i, next);
                i = next;
            }
        }

        void heapify_up()
        {
            int i = count-1;
            while (i > 0) {
                const int p = (i-1)/2;
                if (cmp(x[p], x[i])) {
                    return;
                }
                swap_entries(i, p);
                i = p;
            }
        }
        
        void swap_entries(int i, int j) { T tmp=x[i]; x[i]=x[j]; x[j]=tmp; }
        
        Comp cmp;
        T *x;
        int cap;
        int count;        
    };  
}
