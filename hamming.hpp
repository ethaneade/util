#pragma once

#include <limits.h>
#include <stdint.h>

namespace util {    
    
    template <class T>
    unsigned int count_set_bits(T x)
    {
        // Courtesy of Sean Eron Anderson
        x = x - ((x >> 1) & (T)~(T)0/3);
        x = (x & (T)~(T)0/15*3) + ((x >> 2) & (T)~(T)0/15*3);
        x = (x + (x >> 4)) & (T)~(T)0/255*15;
        return (unsigned int)((T)(x * ((T)~(T)0/255)) >> (sizeof(T) - 1) * CHAR_BIT);
    }

    template <class T>
    unsigned int hamming_distance(const T a, const T b)
    {
        return count_set_bits(a ^ b);
    }

    // Counts column-wise bits,
    // where rows are uint32_t xi = x[i] for i = [0,n)
    template <class X>
    void count_set_bits(const X &x, int n, int count[32])
    {
        for (int i=0; i<32; ++i)
            count[i] = 0;

        int p = 0;

        while (n >= 255) {
            uint32_t r[8] = {0,0,0,0,0,0,0,0};
            for (int j=0; j<17; ++j)
            {
                uint32_t e=0,f=0,g=0,h=0;
                uint32_t c,d;
            
                for (int i=0; i<5; ++i) {
                    const uint32_t x0 = x[p], x1 = x[p+1], x2 = x[p+2];
                    c = ((x0 & 0x55555555) +
                         (x1 & 0x55555555) +
                         (x2 & 0x55555555));
                    d = (((x0>>1) & 0x55555555) +
                         ((x1>>1) & 0x55555555) +
                         ((x2>>1) & 0x55555555));
                    p += 3;
                
                    e += c & 0x33333333;
                    f += (c>>2) & 0x33333333;
                    g += d & 0x33333333;
                    h += (d>>2) & 0x33333333;
                }
            

                r[0] += e & 0x0F0F0F0F;
                r[4] += (e>>4) & 0x0F0F0F0F;
                r[2] += f & 0x0F0F0F0F;
                r[6] += (f>>4) & 0x0F0F0F0F;
                r[1] += g & 0x0F0F0F0F;
                r[5] += (g>>4) & 0x0F0F0F0F;
                r[3] += h & 0x0F0F0F0F;
                r[7] += (h>>4) & 0x0F0F0F0F;
            }
        
            int *o = count;
            for (int i=0; i<4; ++i) {
                for (int j=0; j<8; ++j) {
                    o[j] += r[j] & 0xFF;
                    r[j] >>= 8;
                }
                o += 8;
            }
            n -= 255;
        }

    
        while (n >= 15) {
            uint32_t e=0,f=0,g=0,h=0;
            uint32_t c,d;

            for (int i=0; i<5; ++i) {
                const uint32_t x0 = x[p], x1 = x[p+1], x2 = x[p+2];
                c = ((x0 & 0x55555555) +
                     (x1 & 0x55555555) +
                     (x2 & 0x55555555));
                d = (((x0>>1) & 0x55555555) +
                     ((x1>>1) & 0x55555555) +
                     ((x2>>1) & 0x55555555));
                p += 3;
            
                e += c & 0x33333333;
                f += (c>>2) & 0x33333333;
                g += d & 0x33333333;
                h += (d>>2) & 0x33333333;
            }

            int *o = count;
            for (int i=0; i<8; ++i) {
                o[0] += e & 0xF;
                o[1] += g & 0xF;
                o[2] += f & 0xF;
                o[3] += h & 0xF;
                o += 4;
                e >>= 4;
                f >>= 4;
                g >>= 4;
                h >>= 4;
            }
            n -= 15;        
        }

        while (n >= 3) {
            uint32_t c,d;
            const uint32_t x0 = x[p], x1 = x[p+1], x2 = x[p+2];
            c = ((x0 & 0x55555555) +
                 (x1 & 0x55555555) +
                 (x2 & 0x55555555));
            d = (((x0>>1) & 0x55555555) +
                 ((x1>>1) & 0x55555555) +
                 ((x2>>1) & 0x55555555));
            p += 3;
        
            int *o = count;
            for (int i=0; i<16; ++i) {
                o[0] += c & 0x3;
                o[1] += d & 0x3;
                c >>= 2;
                d >>= 2;
                o += 2;
            }
        
            n -= 3;
        }
        
        for (int i=0; i<n; ++i) {
            uint32_t xi = x[p++];
            for (int j=0; j<8; ++j, xi>>=4) {
                count[j*4+0] += (xi & 1) >> 0;
                count[j*4+1] += (xi & 2) >> 1;
                count[j*4+2] += (xi & 4) >> 2;
                count[j*4+3] += (xi & 8) >> 3;
            }
        }    
    }        
}
