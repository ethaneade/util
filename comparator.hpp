#pragma once

namespace util {

    struct LessThan
    {
        template <typename T>
        bool operator()(const T &a, const T &b) const {
            return a < b;
        }
    };
    
    struct GreateThan
    {
        template <typename T>
        bool operator()(const T &a, const T &b) const {
            return a > b;
        }
    };

}
