#pragma once

namespace util
{
    template <bool Cond> struct StaticAssert;
    template <> struct StaticAssert<true> {};
}

#define UTIL_STATIC_ASSERT(x) (util::StaticAssert<(x)>())
