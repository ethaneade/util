#pragma once

namespace util
{
    double get_time();

    struct Timer
    {
        double start;
        double elapsed() const { return get_time() - start; }
        double reset() { double now = get_time(); start = now; return now; }
        
        Timer() : start(get_time()) {}
    };
    
}
