#pragma once

#include <cstddef>

namespace util {
    
template <class T, int BlockSize=4024>
class Pool
{
private:
    struct Entry {
        union {
            Entry *next;
            char data[sizeof(T)];
        };
    };

    struct Block {
        enum { SIZE = sizeof(Entry) <= BlockSize ? BlockSize/sizeof(Entry) : 1};
        Block *next;
        Entry entries[SIZE];
    };

    Block *blocks;
    Entry *freelist;
    size_t live_count, alloc_count;

public:
    Pool() {
        blocks = 0;
        freelist = 0;
        live_count = 0;
        alloc_count = 0;
    }
    ~Pool() {
        while (blocks) {
            Block *b = blocks;
            blocks = blocks->next;
            delete b;
        }
    }
    
    T *alloc() {
        if (!freelist) {
            Block *b = new Block;
            for (int i=0; i<Block::SIZE; ++i) {
                b->entries[i].next = freelist;
                freelist = &b->entries[i];
            }
            b->next = blocks;
            blocks = b;
            alloc_count += Block::SIZE;
        }
        Entry *next = freelist->next;
        T *value = (T*)freelist->data;
        new (freelist->data) T;
        freelist = next;
        ++live_count;
        return value;
    }

    void release(T *value) {
        Entry *entry = (Entry*)value;
        value->~T();
        entry->next = freelist;
        --live_count;
        freelist = entry;
    }

    void clear() {
        freelist = 0;
        Block *b = blocks;
        while (b) {
            for (int i=0; i<Block::SIZE; ++i) {
                b->entries[i].next = freelist;
                freelist = &b->entries[i];
            }
            b = b->next;            
        }
        live_count = 0;
    }

    size_t num_alive() const { return live_count; }

    size_t size() const { return alloc_count; }
};

}
