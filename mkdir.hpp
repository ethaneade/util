#pragma once

#include <sys/stat.h>

namespace util {
    
    static inline bool mkdir(const char *dirname)
    {
#if _WIN32
        return 0 == ::mkdir(dirname);
#else
        return 0 == ::mkdir(dirname, 0755);
#endif
    }
    
}
